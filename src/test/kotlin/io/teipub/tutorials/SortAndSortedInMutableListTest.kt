package io.teipub.tutorials

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SortAndSortedInMutableListTest {
    @Test
    fun sortTest() {
        val originals = mutableListOf(4, 2, 3, 1)
        originals.sortBy { it }

        assertThat(originals).isEqualTo(mutableListOf(1, 2, 3, 4))
    }


    @Test
    fun sortedTest() {
        val originals = mutableListOf(4, 2, 3, 1)
        val sorted = originals.sortedBy { it }

        assertThat(originals).isEqualTo(mutableListOf(4, 2, 3, 1))
        assertThat(sorted).isEqualTo(mutableListOf(1, 2, 3, 4))
    }

}